﻿using System;

namespace week_05_intro_to_mvc_and_polymorphism
{
    public static void Main(string[] args)
    {
        public class Shape
        {
            public virtual void Draw()
            {
                Console.WriteLine("Base class draw method");
            }
        }

        class Circle : Shape
        {
            public override void Draw()
            {
                Console.WriteLine("Circle");
                base.Draw();
            }
        }

        class Rectangle : Shape
        {
            public override void Draw()
            {
                Console.WriteLine("Rectangle");
                base.Draw();
            }
        }
    
    }   
}



    

